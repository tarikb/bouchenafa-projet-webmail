<?php

namespace App\Controller;

use App\Entity\Email;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/webmail", name="webmail")
 */
class SendmailController extends Controller
{
    /**
     * @Route("/", name="mails")
     */
    public function index(Request $request)
    {
        $mails = $this->getDoctrine()
            ->getRepository(Email::class)
            ->findAll();

        $email = new Email();

        $form = $this->createFormBuilder($email)
            ->add('sender', TextType::class)
            ->add('recipient', TextType::class)
            ->add('subject', TextType::class)
            ->add('message', TextareaType::class)
            ->add('send', SubmitType::class, array('label' => 'Envoyer'))
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($email);
            $em->flush();


        }

        return $this->render('sendmail/index.html.twig', [
            'form' => $form->createView(), 'mails' => $mails,
        ]);

    }

    /**
     * @Route("/show/{id}", name="show", methods="GET")
     */
    public function show($id)
    {

        $mails = $this->getDoctrine()
            ->getRepository(Email::class)
            ->find($id);

        return $this->render('sendmail/mail.html.twig', [
            'mails' => $mails

        ]);
    }


}
